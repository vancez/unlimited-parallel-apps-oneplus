package com.vznoob.unlimitedparallelapps

import android.content.Context
import de.robv.android.xposed.IXposedHookLoadPackage
import de.robv.android.xposed.XC_MethodReplacement
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers.findAndHookMethod
import de.robv.android.xposed.callbacks.XC_LoadPackage

class Hook : IXposedHookLoadPackage {
    override fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam) {
        if (lpparam.packageName != "com.android.settings") {
            return
        }

        try {
            findAndHookMethod(
                lpparam.classLoader.loadClass("com.oneplus.settings.apploader.OPApplicationLoader"),
                "multiAppPackageExcludeFilter",
                Context::class.java, String::class.java,
                XC_MethodReplacement.returnConstant(true)
            )
        } catch (e: ClassNotFoundException) {
            XposedBridge.log(e)
        }
    }
}
